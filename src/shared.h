#ifndef CUBE3D_SHARED
#define CUBE3D_SHARED

#include <malloc.h>
#include <stddef.h>
#include <stdio.h>

#include <raylib.h>
#include <raymath.h>

////////////////////////////////
// Window settings
static const char* wndTitle = "cube3d";
static const int wndWidth = 800;
static const int wndHeight = 600;
static const int wndTargetFPS = 60;

////////////////////////////////
// Shared types

// Communication function type
typedef void(*Function)(void);

// User action data structure
typedef struct {
   // Key is need to be pressed
   KeyboardKey key;
   // Action that executes, when key is pressed
   Function    act;
} UserAction;

// Application states
typedef enum {
   InteractiveMode,
   SettingsMode,

   MODULECOUNT
} State;
static const size_t countModules = (size_t)(MODULECOUNT);

// States module description
typedef struct {
   // Module initialization
   Function Init;
   // Data update function of the module
   Function Update;
   // Drawing function of the module
   Function Redraw;
   // Module cleaning
   Function Purge;
} Module;

// Application colorscheme structure
typedef struct {
   Color FG;
   Color BG;
   Color SL;
} Colorscheme;

// Application settings
typedef struct {
   // Current colorscheme
   Colorscheme colorScheme;
   // Speed of scaling
   float deltaScaling;
   // Speed of rotation
   float deltaRotating;
   // Rasterization switch
   bool Rasterize;
} Settings;

// Defined in main.c
void SwitchState(State state); 
// Defined in settings.c
Module      GetSettingsModule(void);   
Colorscheme GetColorscheme(void);      
float       GetDeltaScaling(void);     
float       GetDeltaRotating(void);    
bool        IsRasterizationEnabled(void);
// Defined in cube.c
Module GetCubeModule(void); 
////////////////////////////////
#endif // !CUBE3D_SHARED
