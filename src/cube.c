#include "shared.h"
#include <math.h>
#include <raylib.h>
#include <raymath.h>
#include <stdbool.h>
////////////////////////////////////////////
///////////// Module content ///////////////
// Useful constants
static const size_t      cubeSize       = 8;
static const size_t      squareSize     = cubeSize / 2;
static const float       defaultScale   = 100.0f;
static const float       defaultAngle   = 0.0f;
static const Quaternion  Origin         = { (float)wndWidth / 2, (float)wndHeight / 2, 1.0f, 1.0f };

// Module actions
typedef enum {
   ScaleUp,
   ScaleDn,
   
   Xclockw,
   Xcountr,
   
   Yclockw,
   Ycountr,

   Zclockw,
   Zcountr,

   Configr,

   ACTIONSCOUNT
} Action;
static const size_t countActions = (size_t)(ACTIONSCOUNT);

// Type defying
typedef enum {
   Scaling,
   RotatingX,
   RotatingY,
   RotatingZ,

   MatrixCount
} MatrixType;

// Data
Quaternion* Cube           = NULL;
Matrix*     Transformation = NULL;
UserAction* UserActions    = NULL;
Function    DrawRule       = NULL;

//// Functions
static void    CubeInit          (void);
static void    CubeUpdate        (void);
static void    HandleActions     (void);

static void    ChangeScale       (float delta);   
static void    IncreaseScale     (void);
static void    DecreaseScale     (void);

static void    RotateX           (float delta);
static void    RotateClockwiseX  (void);
static void    RotateCounterX    (void);

static void    RotateY           (float delta);
static void    RotateClockwiseY  (void);
static void    RotateCounterY    (void);

static void    RotateZ           (float delta);
static void    RotateClockwiseZ  (void);
static void    RotateCounterZ    (void);

static void    ResetCube         (void);
static Matrix  Pipeline          (void);

static void    CubeRedraw        (void);
static void    DrawWireModel     (void);
static void    RasterCube        (void);

static bool    IsInTriangle      (Vector2 p, Quaternion a, Quaternion b, Quaternion c);
static void    Triangle          (Quaternion v1, Quaternion v2, Quaternion v3, Color color);

static void    Configure         (void);

static void    CubePurge         (void);
////////////////////////////////////////////
Module GetCubeModule(void) {
   Module moduleCube;
   moduleCube.Init = &CubeInit;
   moduleCube.Update = &CubeUpdate;
   moduleCube.Redraw = &CubeRedraw;
   moduleCube.Purge = &CubePurge;

   return moduleCube;
}

void CubeInit(void) {
   ///// Data init //////////////////////////////////////////
   // Cube initialization
   Cube = (Quaternion*)malloc(cubeSize * sizeof(Quaternion));
   ResetCube();

   // Initializing transformation matrices
   Transformation = (Matrix*)malloc((size_t)(MatrixCount) * sizeof(Matrix));
   Transformation[Scaling] = MatrixScale(defaultScale, defaultScale, defaultScale);
   Transformation[RotatingX] = MatrixIdentity();
   Transformation[RotatingY] = MatrixIdentity();
   Transformation[RotatingZ] = MatrixIdentity();

   // Defying user actions
   UserActions = (UserAction*)malloc(countActions * sizeof(UserAction));
   UserActions[ScaleUp] = (UserAction){ KEY_EQUAL, &IncreaseScale };
   UserActions[ScaleDn] = (UserAction){ KEY_MINUS, &DecreaseScale };
   UserActions[Xclockw] = (UserAction){ KEY_W,     &RotateClockwiseX };
   UserActions[Xcountr] = (UserAction){ KEY_Q,     &RotateCounterX };
   UserActions[Yclockw] = (UserAction){ KEY_S,     &RotateClockwiseY };   
   UserActions[Ycountr] = (UserAction){ KEY_A,     &RotateCounterY };   
   UserActions[Zclockw] = (UserAction){ KEY_X,     &RotateClockwiseZ };
   UserActions[Zcountr] = (UserAction){ KEY_Z,     &RotateCounterZ };
   UserActions[Configr] = (UserAction){ KEY_TAB,   &Configure };

   // Defying default drawing rule
   DrawRule = &DrawWireModel;
}

#define CUBE_LOG_ON
static inline void LogCube(void) {
#ifdef CUBE_LOG_ON
   puts("Cube info: ");
   for (int v = 0; v < cubeSize; ++v)
      printf("\tv%d: [%f, %f, %f, %f];\n",
         v, Cube[v].x, Cube[v].y, Cube[v].z, Cube[v].w);
#endif
}

void CubeUpdate(void) {
   static bool lastRuleState = false;

   ResetCube();
   HandleActions();  // Handles user actions

   // Applying transformations
   Matrix matPipeline = Pipeline();
   for (size_t v = 0; v < cubeSize; ++v)
      Cube[v] = QuaternionTransform(Cube[v], matPipeline);

   // Checking for settings update
   bool currRuleState = IsRasterizationEnabled();
   if (lastRuleState != currRuleState)
      DrawRule = currRuleState ? &RasterCube : &DrawWireModel;
   lastRuleState = currRuleState;

   LogCube();
}

void HandleActions(void) {
   for (size_t i = 0; i < (size_t)Configr; ++i)
      if (IsKeyDown(UserActions[i].key))
         UserActions[i].act();

   // Get rid of annoying switch mode button behaviour
   if (IsKeyPressed(UserActions[Configr].key))
      UserActions[Configr].act();
}

void ChangeScale(float delta) {
   Transformation[Scaling] = MatrixMultiply(MatrixScale(delta, delta, delta), Transformation[Scaling]);
}

void IncreaseScale(void) {
   ChangeScale(GetDeltaScaling());
}

void DecreaseScale(void) {
   ChangeScale(1 / GetDeltaScaling());
}

void RotateX(float delta) {
   Transformation[RotatingX] = MatrixMultiply(MatrixRotateX(delta), Transformation[RotatingX]);
}

void RotateClockwiseX(void) {
   RotateX(GetDeltaRotating());
}

void RotateCounterX(void) {
   RotateX(-GetDeltaRotating());
}

void RotateY(float delta) {
   Transformation[RotatingY] = MatrixMultiply(MatrixRotateY(delta), Transformation[RotatingY]);
}

void RotateClockwiseY(void) {
   RotateY(GetDeltaRotating());
}

void RotateCounterY(void) {
   RotateY(-GetDeltaRotating());
}

void RotateZ(float delta) {
   Transformation[RotatingZ] = MatrixMultiply(MatrixRotateZ(delta), Transformation[RotatingZ]);
}

void RotateClockwiseZ(void) {
   RotateZ(GetDeltaRotating());
}

void RotateCounterZ(void) {
   RotateZ(-GetDeltaRotating());
}

void ResetCube(void) {
   // --------------------|  x  |  y  |  z  |  w  |--------//
   Cube[0] = (Quaternion){ -1.0f, -1.0f, -1.0f, 1.0f };
   Cube[1] = (Quaternion){ -1.0f, 1.0f, -1.0f, 1.0f };
   Cube[2] = (Quaternion){ 1.0f, 1.0f, -1.0f, 1.0f };
   Cube[3] = (Quaternion){ 1.0f, -1.0f, -1.0f, 1.0f };
   Cube[4] = (Quaternion){ -1.0f, -1.0f, 1.0f, 1.0f };
   Cube[5] = (Quaternion){ -1.0f, 1.0f, 1.0f, 1.0f };
   Cube[6] = (Quaternion){ 1.0f, 1.0f, 1.0f, 1.0f };
   Cube[7] = (Quaternion){ 1.0f, -1.0f, 1.0f, 1.0f };
}

Matrix Pipeline(void) {
   // Shifting origin to center of the screen;
   Matrix Result = MatrixIdentity();
   Result.m12 = Origin.x;
   Result.m13 = Origin.y;
   
   // Scaling
   Result = MatrixMultiply(Transformation[Scaling], Result);
   // Rotating around x-axis
   Result = MatrixMultiply(Transformation[RotatingX], Result);
   // Rotating around y-axis
   Result = MatrixMultiply(Transformation[RotatingY], Result);
   // Rotating around z-axis
   Result = MatrixMultiply(Transformation[RotatingZ], Result);

   return Result;
}

void CubeRedraw(void) {
   ClearBackground(GetColorscheme().BG);
   DrawRule();
}

void DrawWireModel(void) {
   const Color colorFG = GetColorscheme().FG;
   for (size_t v = 0; v < squareSize; ++v) {
      DrawLine(Cube[v].x, Cube[v].y,
       Cube[(v + 1) % squareSize].x, Cube[(v + 1) % squareSize].y, colorFG);
      DrawLine(Cube[v + squareSize].x, Cube[v + squareSize].y,
       Cube[squareSize + ((v + 1) % squareSize)].x, Cube[squareSize + ((v + 1) % squareSize)].y, colorFG);
      DrawLine(Cube[v].x, Cube[v].y, Cube[v + squareSize].x, Cube[v + squareSize].y, colorFG);
   }
}

void RasterCube(void) {
   // Front
   Triangle(Cube[0], Cube[2], Cube[1], RED);
   Triangle(Cube[0], Cube[2], Cube[3], RED);
   // Back
   Triangle(Cube[4], Cube[6], Cube[5], BLUE);
   Triangle(Cube[4], Cube[6], Cube[7], BLUE);
   // Top
   Triangle(Cube[0], Cube[7], Cube[4], YELLOW);
   Triangle(Cube[0], Cube[7], Cube[3], YELLOW);
   // Bottom
   Triangle(Cube[0], Cube[6], Cube[5], GREEN);
   Triangle(Cube[0], Cube[6], Cube[2], GREEN);
   // Left
   Triangle(Cube[0], Cube[5], Cube[1], MAGENTA);
   Triangle(Cube[0], Cube[5], Cube[4], MAGENTA);
   // Right
   Triangle(Cube[2], Cube[7], Cube[3], ORANGE);
   Triangle(Cube[2], Cube[7], Cube[6], ORANGE);
}

static inline int GetSideRelativity(Vector2 p, Quaternion a, Quaternion b) {
   return (a.y - b.y) * p.x + (b.x - a.x) * p.y + (a.x * b.y - b.x * a.y);
}

bool IsInTriangle(Vector2 p, Quaternion a, Quaternion b, Quaternion c) {
   int side_a = GetSideRelativity(p, a, b);
   int side_b = GetSideRelativity(p, b, c);
   int side_c = GetSideRelativity(p, c, a);
   return   (side_a >= 0 && side_b >= 0 && side_c >= 0) ||
            (side_a <= 0 && side_b <= 0 && side_c <= 0);
}

void Triangle(Quaternion v1, Quaternion v2, Quaternion v3, Color color) {
   const int top     = fminf(v1.y, fminf(v2.y, v3.y));
   const int bot     = fmaxf(v1.y, fmaxf(v2.y, v3.y));
   const int right   = fmaxf(v1.x, fmaxf(v2.x, v3.x));
   const int left    = fminf(v1.x, fminf(v2.x, v3.x));

   for (int y = top; y <= bot; ++y)
      for (int x = left; x <= right; ++x) 
         if (IsInTriangle((Vector2){x, y}, v1, v2, v3))
            DrawPixel(x, y, color);
}

void Configure(void) {
   SwitchState(SettingsMode);
}

void CubePurge(void) {
   free((void*)Cube);
   free((void*)Transformation);
   free((void*)UserActions);
}
