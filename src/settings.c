#include "shared.h"
#include <memory.h>
#include <string.h>

///////////////////////////////////
// Types
typedef void(*Mutation)(void*);
typedef const char*(*ShowValue)(void*);
typedef struct {
   // Option name
   const char* name;
   // Value that is changed
   void* value;
   // The way to display option
   ShowValue Show;
   // Transformation of value, when LArrow is pressed
   Mutation Decrease;
   // Transformation of value, when RArrow is pressed
   Mutation Increase;
} OptionData;

// Available options
typedef enum {
   PickBG,
   PickFG,
   PickSL,
   DScale,
   DRotat,
   Raster,

   OPTIONSCOUNT
} Option;
static const size_t countOptions = (size_t)(OPTIONSCOUNT);

// Settings actions
typedef enum {
   Interc,
   MoveUp,
   MoveDn,
   ChngUp,
   ChngDn,

   ACTIONSCOUNT
} Action;
static const size_t countActions = (size_t)(ACTIONSCOUNT);

// Module data
static Settings    appSettings;
static UserAction* UserActions;
static OptionData* Options;
static Option      currentOption;

#define COUNTCOLORS                9
static const size_t countColors  = 9;
static const Color  colorsAvailable[COUNTCOLORS] = 
{ WHITE, RED, ORANGE, YELLOW, GREEN, BLUE, VIOLET, GRAY, BLACK };
static const char*  namesColors[COUNTCOLORS] =
{ "White", "Red", "Orange", "Yellow", "Green", "Blue", "Violet", "Gray", "Black" };
///////////////////////////////////
// Module functions
static void SettingsInit(void);
static void SettingsUpdate(void);
static void SettingsRedraw(void);
static void SettingsPurge(void);

static void Interact(void);
static void GoUpwards(void);
static void GoDownwards(void);
static void ChangePositive(void);
static void ChangeNegative(void);

static bool IsSameColor(Color clr1, Color clr2);
static size_t ColorFind(Color color);
static void ColorIncrease(void* color);
static void ColorDecrease(void* color);
static const char* ColorShow(void* color);

static void DeltaScalingIncrease(void* value);
static void DeltaScalingDecrease(void* value);
static void DeltaRotatingIncrease(void* value);
static void DeltaRotatingDecrease(void* value);
static const char* FloatShow(void* value);

static void BoolToggle(void* value);
static const char* BoolShow(void* value);
///////////////////////////////////
// Module interaction
Module GetSettingsModule(void) {
   Module moduleSettings;
   moduleSettings.Init     = &SettingsInit;
   moduleSettings.Update   = &SettingsUpdate;
   moduleSettings.Redraw   = &SettingsRedraw;
   moduleSettings.Purge    = &SettingsPurge;

   return moduleSettings;
}

void SettingsInit(void) {
   // Init visualization parameters
   appSettings.colorScheme.BG = BLACK;
   appSettings.colorScheme.FG = WHITE;
   appSettings.colorScheme.SL = YELLOW;
   appSettings.Rasterize = false;
   // Init interaction parameters
   appSettings.deltaScaling = 0.95f;
   appSettings.deltaRotating = 0.04f;
   // Init actions
   UserActions = (UserAction*)malloc(countActions * sizeof(UserAction));
   UserActions[Interc] = (UserAction){ KEY_TAB,    &Interact };
   UserActions[MoveUp] = (UserAction){ KEY_UP,     &GoUpwards };
   UserActions[MoveDn] = (UserAction){ KEY_DOWN,   &GoDownwards };
   UserActions[ChngUp] = (UserAction){ KEY_RIGHT,  &ChangePositive };
   UserActions[ChngDn] = (UserAction){ KEY_LEFT,   &ChangeNegative };
   // Init options
   Options = (OptionData*)malloc(countOptions * sizeof(OptionData));
   Options[PickBG] = (OptionData){ "BG Color",              &appSettings.colorScheme.BG,  &ColorShow, &ColorDecrease,         &ColorIncrease          };
   Options[PickFG] = (OptionData){ "FG Color",              &appSettings.colorScheme.FG,  &ColorShow, &ColorDecrease,         &ColorIncrease          };
   Options[PickSL] = (OptionData){ "Selection Color",       &appSettings.colorScheme.SL,  &ColorShow, &ColorDecrease,         &ColorIncrease          };
   Options[DScale] = (OptionData){ "Scaling Delta",         &appSettings.deltaScaling,    &FloatShow, &DeltaScalingDecrease,  &DeltaScalingIncrease   };
   Options[DRotat] = (OptionData){ "Rotation Delta",        &appSettings.deltaRotating,   &FloatShow, &DeltaScalingDecrease,  &DeltaScalingIncrease   };
   Options[Raster] = (OptionData){ "Enable Rasterization",  &appSettings.Rasterize,       &BoolShow,  &BoolToggle,            &BoolToggle             };
   currentOption = (Option)(0);
}

void SettingsUpdate(void) {
   for (size_t i = 0; i < countActions; ++i)
      if (IsKeyPressed(UserActions[i].key))
         UserActions[i].act();
}

void SettingsRedraw(void) {
   const int      sizeFont    = GetFontDefault().baseSize + 5;
   const int      MarginX     = (int)(((float)wndWidth / 100) * 5);
   const int      MarginY     = (int)(((float)wndHeight / 100) * 5);
   const int      LineSpace   = sizeFont + 3;
   const char     Space       = ' ';
   const size_t   bufsiz      = 128;

   ClearBackground(GetColorscheme().BG);
   for (size_t i = 0; i < countOptions; ++i) {    
      char buffer[bufsiz];
      memset(buffer, Space, bufsiz);
      sprintf(buffer, "%s: %s", Options[i].name, Options[i].Show(Options[i].value));  

      Color color = ((Option)(i) == currentOption) ? GetColorscheme().SL : GetColorscheme().FG;
      DrawText(buffer, MarginX, MarginY + (i * LineSpace), sizeFont, color);
   }
}

void SettingsPurge(void) {
   free((void*)UserActions);
   free((void*)(Options));
}
///////////////////////////////////
// Internal
void Interact(void) {
   SwitchState(InteractiveMode);
}

void GoUpwards(void) {
   size_t i = (size_t)currentOption;
   if ((--i) >= countOptions)
      i = countOptions - 1;
   currentOption = (Option)(i);   
}

void GoDownwards(void) {
   size_t i = (size_t)currentOption;
   if ((++i) >= countOptions)
      i = 0;
   currentOption = (Option)(i);   
}

void ChangePositive(void) {
   OptionData* curOptData = &Options[currentOption];
   curOptData->Increase(curOptData->value);
}

void ChangeNegative(void) {
   OptionData* curOptData = &Options[currentOption];
   curOptData->Decrease(curOptData->value);
}

bool IsSameColor(Color clr1, Color clr2) {
   return   clr1.a == clr2.a &&
            clr1.b == clr2.b &&
            clr1.g == clr2.g &&
            clr1.r == clr2.r;
}

size_t ColorFind(Color color) {
   for (size_t i = 0; i < countColors; ++i)
      if (IsSameColor(color, colorsAvailable[i]))
         return i;
   return countColors;
}

void ColorIncrease(void* color) {
   Color* nice_color = (Color*)(color);
   size_t iColor = ColorFind(*nice_color);
   *nice_color = colorsAvailable[(iColor + 1) % countColors];
}

void ColorDecrease(void* color) {
   Color* nice_color = (Color*)(color);
   size_t iColor = ColorFind(*nice_color);
   iColor = (iColor - 1) > countColors ? countColors - 1 : iColor - 1;
   *nice_color = colorsAvailable[iColor];
} 

const char* ColorShow(void* color) {
   size_t iColor = ColorFind(*(Color*)(color));
   switch (iColor) {
      case COUNTCOLORS: return "undefined";
      default: return namesColors[iColor];
   }
}

void DeltaScalingIncrease(void* value) {
   const float ceil = 1.0f;
   const float step = 0.01f;

   float* nice_value = (float*)value;
   if (*nice_value < ceil)
      (*nice_value) += step;
}

void DeltaScalingDecrease(void* value) {
   const float floor = 0.85f;
   const float step = 0.01f;

   float* nice_value = (float*)value;
   if (*nice_value > floor)
      (*nice_value) -= step;
}

void DeltaRotatingIncrease(void* value) {
   const float ceil = 0.03f;
   const float step = 0.001f;

   float* nice_value = (float*)value;
   if (*nice_value < ceil)
      (*nice_value) += step;
}

void DeltaRotatingDecrease(void* value) {
   const float floor = 0.001f;
   const float step = 0.001f;

   float* nice_value = (float*)value;
   if (*nice_value > floor)
      (*nice_value) -= step;
}

const char* FloatShow(void* value) {
   static char result[9]; // Nine is a number of characters in float
   sprintf(result, "%f", *(float*)(value));
   return result;
}

void BoolToggle(void* value) {
   bool* nice_bool  = (bool*)(value);
   (*nice_bool) = !(*nice_bool);
}

const char* BoolShow(void* value) {
   return *(bool*)(value) ? "true" : "false";
}
///////////////////////////////////
// External
Colorscheme GetColorscheme(void) {
   return appSettings.colorScheme;
}

float GetDeltaScaling(void) {
   return appSettings.deltaScaling;
}

float GetDeltaRotating(void) {
   return appSettings.deltaRotating;
}

bool IsRasterizationEnabled(void) {
   return appSettings.Rasterize;
}
///////////////////////////////////
