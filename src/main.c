#include <stdlib.h>

#include "shared.h"
////////////////////////////////
// Global variables
State       stateCurrent;
Module*     appModules;
////////////////////////////////
// Declaring local functions
static void Init(void);
static void Loop(void);
static int Purge(void);
////////////////////////////////
int main(void) {
   Init();
   Loop();
   return Purge();
}

void Init(void) {
   //// Window init ////
   InitWindow(wndWidth, wndHeight, wndTitle);
   SetTargetFPS(wndTargetFPS);
   // Setup state
   stateCurrent = InteractiveMode;
   // Loading modules
   appModules = (Module*)malloc(countModules * sizeof(Module));
   appModules[InteractiveMode] = GetCubeModule();
   appModules[SettingsMode] = GetSettingsModule();

   for (size_t i = 0; i < countModules; ++i)
      appModules[i].Init();
}

void Loop(void) {
   while (!WindowShouldClose()) {
      appModules[stateCurrent].Update();
      BeginDrawing();
         appModules[stateCurrent].Redraw();
      EndDrawing();
   }
}

int Purge(void) {
   for (size_t i = 0; i < countModules; ++i)
      appModules[i].Purge();
   free((void*)appModules);
   CloseWindow();
   return EXIT_SUCCESS;
}
////////////////////////////////
void SwitchState(State state) {
   stateCurrent = state;
}
