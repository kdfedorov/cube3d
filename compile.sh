# Target info
TARGET_DIR="./target"
EXEC_NAME="cube3d"
OUTPUT="${TARGET_DIR}/${EXEC_NAME}"

# Removing previously compiled program
if [ -f ${OUTPUT} ]; then
   rm -f ${OUTPUT}
fi
# Making target directory if it's not exists
if [ ! -d ${TARGET_DIR} ]; then
   mkdir ${TARGET_DIR}
fi

# Compiler flags
FLAGS="-lraylib -lGL -lm -lpthread -ldl -lrt -lX11"
# Sources directory
SRC_DIR="./src"

# Source files list
SOURCES="
   ${SRC_DIR}/main.c
   ${SRC_DIR}/cube.c
   ${SRC_DIR}/settings.c
"

# Compiling
cc ${SOURCES} ${FLAGS} -o ${OUTPUT}
