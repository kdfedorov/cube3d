# cube3d
## What's this?
This is a cube renderer written in C, with a small amount of configurable parameters like speed of rotation, or background color.

## Techs
Developed using `raylib v.4.0` and `gcc 11.2.0`.

### Building on GNU/Linux
Download latest raylib and gcc using your package manager, e.g.:
`pacman -S gcc raylib`<br>

Launch the `compile.sh`.

### Building on something else
Since the `raylib` is the only external dependency, I suggest you to check out it's [official repo](https://github.com/raysan5/raylib) to check instructions for your working platform.

## Controls
### Interactive mode
`-` - Zoom out.<br>
`=` - Zoom in.<br>
<br>
`q` - Counter-clockwise rotation by X-axis.<br>
`w` - Clockwise rotation by X-axis.<br>
`a` - Counter-clockwise rotation by Y-axis.<br>
`s` - Clockwise rotation by Y-axis.<br>
`z` - Counter-clockwise rotation by Z-axis.<br>
`x` - Clockwise rotation by Z-axis.<br>
<br>
`TAB` - Settings.

### Settings mode
`UP` - Select option above.<br>
`DOWN` - Select option under.<br>
`LEFT` - Decrease value.<br>
`RIGHT` - Increase value.<br>
<br>
`TAB` - Back to Interactive mode.<br>

